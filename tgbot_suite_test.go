// Copyright 2017-2024 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tgbot

import (
	"encoding/json"
	"os"
	"strconv"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

//nolint:gochecknoglobals
var (
	subChatID     int64
	unsubChatID   int64
	telegramToken string
)

func TestTgbot(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Tgbot Suite")
}

func mustUnmarshall(data string, v interface{}) {
	if err := json.Unmarshal([]byte(data), v); err != nil {
		panic(err)
	}
}

func mustNewChatBot() *Bot {
	cb, err := NewBot(telegramToken)
	if err != nil {
		panic(err)
	}

	return cb
}

func getEnvInt64(varname string) int64 {
	envvar, err := strconv.ParseInt(os.Getenv(varname), 10, 0)
	if err != nil {
		panic("Can't parse " + varname)
	}

	return envvar
}

var _ = BeforeSuite(func() {
	subChatID = getEnvInt64("SUBSCRIBED_CHAT_ID")
	unsubChatID = getEnvInt64("UNSUBSCRIBED_CHAT_ID")
	telegramToken = os.Getenv("TELEGRAM_TOKEN")
})

const (
	commandUpdateFixture = `{
  "update_id": 1,
  "message": {
    "message_id": 23,
    "from": {
      "id": 12345678,
      "first_name": "Ivan",
      "last_name": "Ivanov",
      "username": "ivan",
      "language_code": "ru-RU",
      "is_bot": false
    },
    "date": 1514991529,
    "chat": {
      "id": 12345678,
      "type": "private",
      "title": "",
      "username": "ivan",
      "first_name": "Ivan",
      "last_name": "Ivanov",
      "all_members_are_administrators": false,
      "photo": null
    },
    "forward_from": null,
    "forward_from_chat": null,
    "forward_from_message_id": 0,
    "forward_date": 0,
    "reply_to_message": null,
    "edit_date": 0,
    "text": "/command",
    "entities": [
      {
        "type": "bot_command",
        "offset": 0,
        "length": 8,
        "url": "",
        "user": null
      }
    ],
    "audio": null,
    "document": null,
    "game": null,
    "photo": null,
    "sticker": null,
    "video": null,
    "video_note": null,
    "voice": null,
    "caption": "",
    "contact": null,
    "location": null,
    "venue": null,
    "new_chat_members": null,
    "left_chat_member": null,
    "new_chat_title": "",
    "new_chat_photo": null,
    "delete_chat_photo": false,
    "group_chat_created": false,
    "supergroup_chat_created": false,
    "channel_chat_created": false,
    "migrate_to_chat_id": 0,
    "migrate_from_chat_id": 0,
    "pinned_message": null,
    "invoice": null,
    "successful_payment": null
  },
  "edited_message": null,
  "channel_post": null,
  "edited_channel_post": null,
  "inline_query": null,
  "chosen_inline_result": null,
  "callback_query": null,
  "shipping_query": null,
  "pre_checkout_query": null
}`

	inlineButtonCallbackUpdateFixture = `{
  "update_id": 337351857,
  "message": null,
  "edited_message": null,
  "channel_post": null,
  "edited_channel_post": null,
  "inline_query": null,
  "chosen_inline_result": null,
  "callback_query": {
    "id": "426559891468128660",
    "from": {
      "id": 12345678,
      "first_name": "Ivan",
      "last_name": "Ivanov",
      "username": "ivan",
      "language_code": "ru-RU",
      "is_bot": false
    },
    "message": {
      "message_id": 31,
      "from": {
        "id": 529060320,
        "first_name": "message_dump_bot",
        "last_name": "",
        "username": "message_dump_bot",
        "language_code": "",
        "is_bot": true
      },
      "date": 1515016260,
      "chat": {
        "id": 12345678,
        "type": "private",
        "title": "",
        "username": "ivan",
        "first_name": "Ivan",
        "last_name": "Ivanov",
        "all_members_are_administrators": false,
        "photo": null
      },
      "forward_from": null,
      "forward_from_chat": null,
      "forward_from_message_id": 0,
      "forward_date": 0,
      "reply_to_message": null,
      "edit_date": 0,
      "text": "Inline keyboard",
      "entities": null,
      "audio": null,
      "document": null,
      "game": null,
      "photo": null,
      "sticker": null,
      "video": null,
      "video_note": null,
      "voice": null,
      "caption": "",
      "contact": null,
      "location": null,
      "venue": null,
      "new_chat_members": null,
      "left_chat_member": null,
      "new_chat_title": "",
      "new_chat_photo": null,
      "delete_chat_photo": false,
      "group_chat_created": false,
      "supergroup_chat_created": false,
      "channel_chat_created": false,
      "migrate_to_chat_id": 0,
      "migrate_from_chat_id": 0,
      "pinned_message": null,
      "invoice": null,
      "successful_payment": null
    },
    "inline_message_id": "",
    "chat_instance": "-6259769415495886535",
    "data": "data:ok",
    "game_short_name": ""
  },
  "shipping_query": null,
  "pre_checkout_query": null
}`
)
