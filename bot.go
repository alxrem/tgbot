// Copyright 2017-2024 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tgbot

import (
	"context"
	"errors"
	"log/slog"
	"sync"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type Bot struct {
	bot            *tgbotapi.BotAPI
	sendBot        *SendBot
	updates        <-chan tgbotapi.Update
	wg             sync.WaitGroup
	mu             sync.Mutex
	ctx            context.Context
	cancel         context.CancelFunc
	handlers       map[string]HandlerFunc
	routers        []router
	defaultHandler HandlerFunc
}

var (
	errNewBotAPI  = errors.New("failed to create new Bot API")
	errNewSendBot = errors.New("failed to create new send bot")
)

func NewBot(token string) (*Bot, error) {
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		return nil, errors.Join(errNewBotAPI, err)
	}

	sb, err := NewSendBot(token)
	if err != nil {
		return nil, errors.Join(errNewSendBot, err)
	}

	return &Bot{
		bot:      bot,
		sendBot:  sb,
		handlers: make(map[string]HandlerFunc),
		routers:  make([]router, 0),
	}, nil
}

func (b *Bot) Run(ctx context.Context) {
	slog.Info("Starting bot...")

	b.ctx, b.cancel = context.WithCancel(ctx)

	b.wg.Add(1)

	go func() {
		b.sendBot.Run(ctx)
		b.wg.Done()
	}()

	var err error

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	b.updates, err = b.bot.GetUpdatesChan(u)
	if err != nil {
		// now this is impossible condition because `GetUpdatesChan` always returns `updatesChan, nil`
		panic(err)
	}

	for {
		select {
		case <-b.ctx.Done():
			slog.Debug("Bot canceled")
			return
		case update := <-b.updates:
			b.wg.Add(1)

			go func(u tgbotapi.Update) {
				b.route(u)
				b.wg.Done()
			}(update)
		}
	}
}

func (b *Bot) Stop() {
	slog.Info("Stopping bot...")
	b.cancel()
	b.sendBot.Stop()
	b.wg.Wait()
	slog.Debug("Bot stopped")
}

func (b *Bot) Self() tgbotapi.User {
	return b.bot.Self
}

func (b *Bot) Send(c tgbotapi.Chattable, callback ...Callback) {
	b.sendBot.Send(c, callback...)
}
