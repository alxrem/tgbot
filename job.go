// Copyright 2017-2024 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tgbot

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

type Status int

const (
	StatusOK        Status = 200
	StatusForbidden Status = 403
	StatusError     Status = 500
)

type Callback func(status Status, response tgbotapi.Message, err error)

type job struct {
	message  tgbotapi.Chattable
	retries  int
	callback Callback
}

func newJob(c tgbotapi.Chattable, callback ...Callback) job {
	j := job{
		message: c,
		retries: 0,
	}
	if len(callback) > 0 {
		j.callback = callback[0]
	}

	return j
}

func (j *job) result(status Status, response tgbotapi.Message, err error) {
	if j.callback != nil {
		j.callback(status, response, err)
	}
}
