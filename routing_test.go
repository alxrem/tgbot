// Copyright 2017-2024 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tgbot

import (
	"context"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Routing", func() {
	var cb *Bot
	var commandUpdate tgbotapi.Update

	BeforeEach(func() { cb = mustNewChatBot() })

	Context("without defined routes", func() {
		JustBeforeEach(func() { cb.route(commandUpdate) })

		It("should cause no errors", func() {})
	})

	Context("with defined command route", func() {
		var calledCommand string
		var commandArgs []string

		const expectedCommand = "command"

		BeforeEach(func() {
			mustUnmarshall(commandUpdateFixture, &commandUpdate)
			calledCommand = ""
			commandArgs = nil
			cb.RouteCommand(expectedCommand, func(_ context.Context, update tgbotapi.Update, args ...string) {
				calledCommand = expectedCommand
				commandArgs = args
			})
		})

		JustBeforeEach(func() { cb.route(commandUpdate) })

		Context("unregistered command", func() {
			It("should cause no errors", func() {})

			Context("with defined default handler", func() {
				var calledHandler string

				const calledHandlerExpected = "default"

				BeforeEach(func() {
					commandUpdate.Message.Text = "/unregistered"
					calledHandler = ""
					cb.RouteDefault(func(_ context.Context, update tgbotapi.Update, args ...string) {
						calledHandler = calledHandlerExpected
					})
				})

				It("should call default handler", func() {
					Expect(calledHandler).To(Equal(calledHandlerExpected))
				})
			})
		})

		Context("registered command", func() {
			Context("without arguments", func() {
				BeforeEach(func() {
					commandUpdate.Message.Text = "/" + expectedCommand
				})

				It("should call handler of /command with no args", func() {
					Expect(commandArgs).NotTo(BeNil())
					Expect(calledCommand).To(Equal(expectedCommand))
					Expect(commandArgs).To(BeEmpty())
				})
			})

			Context("with 2 arguments", func() {
				BeforeEach(func() {
					commandUpdate.Message.Text = "/" + expectedCommand + " arg1 arg2"
				})

				It(`should call handler of /command with args {"arg1", "arg2"}`, func() {
					Expect(commandArgs).NotTo(BeNil())
					Expect(calledCommand).To(Equal(expectedCommand))
					Expect(commandArgs).To(HaveLen(2))
					Expect(commandArgs[0]).To(Equal("arg1"))
					Expect(commandArgs[1]).To(Equal("arg2"))
				})
			})
		})
	})

	Context("with defined callback route", func() {
		var inlineButtonCallbackUpdate tgbotapi.Update
		var callbackArgs []string
		var calledCallback string
		const expectedCallback = `data:ok`

		BeforeEach(func() {
			mustUnmarshall(inlineButtonCallbackUpdateFixture, &inlineButtonCallbackUpdate)
			inlineButtonCallbackUpdate.CallbackQuery.Data = "data:ok"
			calledCallback = ""
			callbackArgs = nil
			cb.RouteCallback(`data:ok`, func(_ context.Context, update tgbotapi.Update, args ...string) {
				calledCallback = expectedCallback
				callbackArgs = args
			})
		})

		JustBeforeEach(func() {
			cb.route(inlineButtonCallbackUpdate)
		})

		Context("without captures", func() {
			It("should call callback with empty array of args", func() {
				Expect(calledCallback).To(Equal(expectedCallback))
				Expect(callbackArgs).NotTo(BeNil())
				Expect(callbackArgs).To(BeEmpty())
			})
		})

		Context("with capture", func() {
			const expectedCallback = `data:(\d+)`

			BeforeEach(func() {
				inlineButtonCallbackUpdate.CallbackQuery.Data = "data:123"
				cb.RouteCallback(expectedCallback, func(ctx context.Context, update tgbotapi.Update, args ...string) {
					calledCallback = expectedCallback
					callbackArgs = args
				})
			})

			It(`should call callback with single argument {"123"}`, func() {
				Expect(calledCallback).To(Equal(expectedCallback))
				Expect(callbackArgs).NotTo(BeNil())
				Expect(callbackArgs).To(HaveLen(1))
				Expect(callbackArgs[0]).To(Equal("123"))
			})
		})
	})
})
