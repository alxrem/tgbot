// Copyright 2017-2024 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tgbot

import (
	"context"
	"errors"
	"strings"
	"sync"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"golang.org/x/time/rate"
)

type SendBot struct {
	bot      *tgbotapi.BotAPI
	ctx      context.Context
	cancel   context.CancelFunc
	lim      *rate.Limiter
	wg       sync.WaitGroup
	messages chan job
}

func NewSendBot(token string) (*SendBot, error) {
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		return nil, errors.Join(errNewBotAPI, err)
	}

	return &SendBot{
		bot:      bot,
		lim:      rate.NewLimiter(30, 1),
		messages: make(chan job),
	}, nil
}

func (sb *SendBot) Run(ctx context.Context) {
	sb.ctx, sb.cancel = context.WithCancel(ctx)

	sb.wg.Add(1)

	go func() {
		defer sb.wg.Done()

		for {
			select {
			case <-sb.ctx.Done():
				return
			case job := <-sb.messages:
				sb.send(job)
			}
		}
	}()
}

func (sb *SendBot) Stop() {
	sb.cancel()
	sb.wg.Wait()
}

func (sb *SendBot) Send(c tgbotapi.Chattable, callback ...Callback) {
	sb.schedule(newJob(c, callback...), 0)
}

func (sb *SendBot) schedule(j job, delay time.Duration) {
	sb.wg.Add(1)

	go func() {
		defer sb.wg.Done()

		select {
		case <-time.After(delay):
			select {
			case sb.messages <- j:
			case <-sb.ctx.Done():
			}
		case <-sb.ctx.Done():
		}
	}()
}

func (sb *SendBot) send(j job) {
	r := sb.lim.Reserve()
	if r.OK() {
		time.Sleep(r.Delay())

		response, err := sb.bot.Send(j.message)
		if err == nil {
			j.result(StatusOK, response, nil)
			return
		}

		switch {
		case strings.HasPrefix(err.Error(), "Too Many Requests"):
		case strings.HasSuffix(err.Error(), "chat not found"):
			j.result(StatusForbidden, response, err)
			return
		default:
			j.result(StatusError, response, err)
			return
		}
	}

	j.retries += 1

	sb.schedule(j, 300*time.Second)
}
