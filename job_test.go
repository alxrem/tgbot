// Copyright 2017-2024 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tgbot

import (
	"errors"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Job", func() {
	var job job

	Context("without callback", func() {
		BeforeEach(func() {
			job = newJob(tgbotapi.NewMessage(1, "test"))
		})

		It("should create job with nil callback", func() {
			Expect(job.retries).To(Equal(0))
			Expect(job.callback).To(BeNil())
		})

		It("should not try to callback", func() {
			job.result(StatusOK, tgbotapi.Message{}, nil)
		})
	})

	Context("with callback", func() {
		var status Status
		var responseText string
		var response tgbotapi.Message
		var expectedResponse tgbotapi.Message
		var err error
		var errExpected error

		BeforeEach(func() {
			status = -1
			responseText = "test"
			expectedResponse = tgbotapi.Message{
				MessageID: 1234,
				Text:      responseText,
			}
			errExpected = errors.New("testing error") //nolint:err113
			job = newJob(tgbotapi.NewMessage(1, responseText), func(r Status, m tgbotapi.Message, e error) {
				response = m
				status = r
				err = e
			})
		})

		It("should create job with callback", func() {
			Expect(job.retries).To(Equal(0))
			Expect(job.callback).NotTo(BeNil())
		})

		It("should run callback and assign status with no error", func() {
			job.result(StatusOK, expectedResponse, nil)
			Ω(err).ShouldNot(HaveOccurred())
			Expect(status).To(Equal(StatusOK))
			Expect(response).To(Equal(expectedResponse))
		})

		It("should run callback and assign result with an error", func() {
			job.result(StatusError, expectedResponse, errExpected)
			Ω(err).Should(HaveOccurred())
			Expect(err).To(Equal(errExpected))
			Expect(status).To(Equal(StatusError))
			Expect(response).To(Equal(expectedResponse))
		})
	})
})
