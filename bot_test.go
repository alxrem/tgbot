// Copyright 2017-2024 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tgbot

import (
	"os"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Bot", func() {
	var (
		cb    *Bot
		err   error
		token string
	)

	Describe("constructor", func() {
		JustBeforeEach(func() {
			cb, err = NewBot(token)
		})
		Context("with valid token", func() {
			BeforeEach(func() {
				token = os.Getenv("TELEGRAM_TOKEN")
			})

			It("should create bot without errors", func() {
				Ω(err).ShouldNot(HaveOccurred())
				Ω(cb).ShouldNot(BeNil())
			})
		})
		Context("with invalid token", func() {
			BeforeEach(func() {
				token = "invalid token"
			})

			It("should fail", func() {
				Ω(err).Should(HaveOccurred())
				Ω(cb).Should(BeNil())
			})
		})
	})
})
