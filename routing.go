// Copyright 2017-2024 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tgbot

import (
	"context"
	"regexp"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type (
	HandlerFunc func(ctx context.Context, update tgbotapi.Update, args ...string)
	router      func(update tgbotapi.Update) bool
)

func (b *Bot) appendRouter(router router) {
	b.mu.Lock()
	defer b.mu.Unlock()
	b.routers = append(b.routers, router)
}

var spaceRE = regexp.MustCompile(`\s+`)

func (b *Bot) RouteCommand(command string, handler HandlerFunc) {
	b.appendRouter(func(update tgbotapi.Update) bool {
		if !(update.Message != nil && update.Message.IsCommand() && update.Message.Command() == command) {
			return false
		}

		var args []string

		cArgs := update.Message.CommandArguments()
		if cArgs == "" {
			args = make([]string, 0)
		} else {
			args = spaceRE.Split(cArgs, -1)
		}

		handler(b.ctx, update, args...)

		return true
	})
}

func (b *Bot) RouteCallback(reStr string, handler HandlerFunc) {
	re := regexp.MustCompile(reStr)

	b.appendRouter(func(update tgbotapi.Update) bool {
		if update.CallbackQuery == nil {
			return false
		}

		if matches := re.FindStringSubmatch(update.CallbackQuery.Data); len(matches) > 0 {
			args := matches[1:]
			handler(b.ctx, update, args...)

			return true
		}

		return false
	})
}

func (b *Bot) RouteDefault(handler HandlerFunc) {
	b.defaultHandler = handler
}

func (b *Bot) route(update tgbotapi.Update) {
	for _, router := range b.routers {
		if router(update) {
			return
		}
	}

	if b.defaultHandler != nil {
		b.defaultHandler(b.ctx, update)
	}
}
