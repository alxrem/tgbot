// Copyright 2017-2024 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tgbot

import (
	"context"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

type jobResult struct {
	status   Status
	response tgbotapi.Message
	err      error
}

var _ = Describe("SendBot", func() {
	var sb *SendBot
	var err error

	Describe("constructor", func() {
		JustBeforeEach(func() { sb, err = NewSendBot(telegramToken) })

		Context("with valid token", func() {
			It("should create bot without errors", func() {
				Ω(err).ShouldNot(HaveOccurred())
				Ω(sb).ShouldNot(BeNil())
			})
		})
		Context("with invalid token", func() {
			var tokenBak string

			BeforeEach(func() {
				tokenBak = telegramToken
				telegramToken = "invalid token"
			})

			AfterEach(func() { telegramToken = tokenBak })

			It("should fail", func() {
				Ω(err).Should(HaveOccurred())
				Ω(sb).Should(BeNil())
			})
		})
	})

	Describe("workflow", func() {
		JustBeforeEach(func() {
			sb, err = NewSendBot(telegramToken)
			if err != nil {
				panic(err)
			}
			sb.Run(context.Background())
		})

		Describe("starting and stopping", func() {
			It("should start and stop bot", func() {
				timer := time.NewTimer(5 * time.Second)
				ticker := time.NewTicker(100 * time.Millisecond)
				select {
				case <-timer.C:
					Fail("Bot is not started in 5 seconds")
				case <-ticker.C:
					if sb.cancel != nil {
						break
					}
				}
				sb.Stop()
				Eventually(sb.ctx.Done()).Should(BeClosed())
			})
		})

		Describe("sending of message", func() {
			var (
				chatID int64
				text   = "text"
			)

			AfterEach(func() { sb.Stop() })

			Context("without callback", func() {
				Context("to subscribed channel", func() {
					BeforeEach(func() { chatID = subChatID })
					It("should run without errors", func() {
						sb.Send(tgbotapi.NewMessage(chatID, text))
					})
				})

				Context("to unsubscribed channel", func() {
					BeforeEach(func() { chatID = unsubChatID })
					It("should run without errors", func() {
						sb.Send(tgbotapi.NewMessage(chatID, text))
					})
				})
			})

			Context("with callback", func() {
				var result chan jobResult

				JustBeforeEach(func() {
					result = make(chan jobResult)
					sb.Send(tgbotapi.NewMessage(chatID, text), func(s Status, r tgbotapi.Message, e error) {
						jr := new(jobResult)
						jr.status = s
						jr.response = r
						jr.err = e
						result <- *jr
						close(result)
					})
				})

				Context("to subscribed channel", func() {
					BeforeEach(func() { chatID = subChatID })

					It("should assign StatusOK to status", func() {
						jr := <-result
						Ω(jr.err).ShouldNot(HaveOccurred())
						Expect(jr.status).Should(Equal(StatusOK))
						Expect(jr.response.Chat.ID).Should(Equal(chatID))
						Expect(jr.response.Text).Should(Equal(text))
					})
				})

				Context("to unsubscribed channel", func() {
					BeforeEach(func() { chatID = unsubChatID })

					It("should assign StatusForbidden to status", func() {
						jr := <-result
						Ω(jr.err).Should(HaveOccurred())
						Expect(jr.status).Should(Equal(StatusForbidden))
					})
				})
			})
		})
	})
})
