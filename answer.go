// Copyright 2017-2024 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package tgbot

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func (b *Bot) AnswerPlain(update tgbotapi.Update, message string) {
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, message)
	b.Send(msg)
}

func (b *Bot) Answer(update tgbotapi.Update, message string) {
	b.AnswerWithMarkup(update, message, nil)
}

func (b *Bot) AnswerRemovingKeyboard(update tgbotapi.Update, message string) {
	b.AnswerWithMarkup(update, message, tgbotapi.NewRemoveKeyboard(true))
}

func (b *Bot) AnswerWithKeyboard(update tgbotapi.Update, message string, captions []string) {
	keyboard := make([][]tgbotapi.KeyboardButton, len(captions)+1)
	for i, caption := range captions {
		keyboard[i] = []tgbotapi.KeyboardButton{tgbotapi.NewKeyboardButton(caption)}
	}

	keyboard[len(captions)] = []tgbotapi.KeyboardButton{tgbotapi.NewKeyboardButton("/close")}
	b.AnswerWithMarkup(update, message, tgbotapi.ReplyKeyboardMarkup{
		ResizeKeyboard: true,
		Keyboard:       keyboard,
	})
}

func (b *Bot) AnswerWithMarkup(update tgbotapi.Update, message string, replyMarkup interface{}) {
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, message)
	msg.ReplyMarkup = replyMarkup
	msg.ParseMode = tgbotapi.ModeMarkdown
	b.Send(msg)
}
