// Copyright 2017-2024 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"sync"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/alxrem/tgbot"
)

func main() {
	os.Exit(Main())
}

func Main() int {
	telegramToken := os.Getenv("TELEGRAM_TOKEN")

	chatID, err := strconv.ParseInt(os.Getenv("TELEGRAM_CHAT_ID"), 10, 64)
	if err != nil {
		panic(err)
	}

	sb, err := tgbot.NewSendBot(telegramToken)
	if err != nil {
		panic(err)
	}

	var wg sync.WaitGroup

	wg.Add(1)

	go func() {
		sb.Run(context.Background())
		wg.Done()
	}()

	for i := range 1 {
		msg := tgbotapi.NewMessage(chatID, fmt.Sprintf("msg #%d", i+1))
		sb.Send(msg)
	}

	sb.Stop()
	wg.Wait()

	return 0
}
